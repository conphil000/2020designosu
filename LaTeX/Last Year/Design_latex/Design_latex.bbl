\begin{thebibliography}{10}

\bibitem{web:pay}
{51-8091 Chemical Plant and System Operators. (2018, March 30). Retrieved from
  https://www.bls.gov/oes/2017/may/oes518091.htm.}

\bibitem{web:HEX}
{Bell, K. J., \& Ramsey, J. D. (n.d.). Heat Exchangers for the Process and
  Energy Industries. Heat Exchangers for the Process and Energy Industries.}

\bibitem{web:perms}
{Caldwell, E. (2019, April 2). The Permian Basin Is Now the Highest Producing
  Oilfield in the World. Retrieved from
  https://www.energyindepth.org/the-permian-basin-is-now-the-highest-producing-oilfield-in-the-world/.}

\bibitem{web:CEPCI}
{Chemical Engineering Plant Cost Index - Chemical Engineering. (n.d.).
  Retrieved from https://www.chemengonline.com/pci.}

\bibitem{web:perm}
{Conca, J. (2017, April 13). Permania: 100 Years In The Permian Oil Fields Of
  Texas And New Mexico. Retrieved from
  https://www.forbes.com/sites/jamesconca/2017/04/11/permania-100-years-in-the-permian-oil-fields-of-new-mexico-and-texas/\#374cfe9d6970.}

\bibitem{web:crude}
{Historical Crude Oil Prices (Table). (n.d.). Retrieved from
  https://inflationdata.com/articles/inflation-adjusted-prices/historical-crude-oil-prices-table/.}

\bibitem{web:tox}
{International Chemical Safety Cards. (n.d.). Retrieved from
  https://www.ilo.org/dyn/icsc/showcard.listcards3?p\_lang=en.}

\bibitem{web:natgas}
{Natural Gas Prices - Historical Chart. (n.d.). Retrieved from
  https://www.macrotrends.net/2478/natural-gas-prices-historical-chart.}

\bibitem{web:steel}
{Producer Price Index by Commodity for Metals and Metal Products: Cold Rolled
  Steel Sheet and Strip. (2019, November 14). Retrieved from
  https://fred.stlouisfed.org/series/WPU101707.}

\bibitem{web:CPI}
{Producer Price Index by Industry: Utilities. (2019, November 14). Retrieved
  from https://fred.stlouisfed.org/series/PCU221221.}

\bibitem{web:spdr}
{SPDR S\&P 500 (SPY) Stock Historical Prices \& Data. (2019, November 21).
  Retrieved from
  \url{https://finance.yahoo.com/quote/SPY/history?period1=1416204000\&period2=1573970400\&interval=1d\&filter=history\&frequency=1d.}}

\bibitem{web:osha}
{UNITED STATES DEPARTMENT OF LABOR. (n.d.). Retrieved from
  https://www.osha.gov/.}

\bibitem{web:water}
{U.S. Department of Energy. (2017). Water and Wastewater Annual Price
  Escalation Rates for Selected Cities across the United States. Retrieved from
  \url{https://www.energy.gov/sites/prod/files/2017/10/f38/water_wastewater_escalation_rate\_study.pdf}}.

\bibitem{web:gasoline}
{U.S. Energy Information Administration - EIA - Independent Statistics and
  Analysis. (n.d.). Retrieved from https://www.eia.gov/petroleum/gasdiesel/.}

\bibitem{book:GPSA}
{Gas Processors Suppliers Association (U.S.) and Gas Processors Association.}
\newblock {\em GPSA ENGINEERING DATA BOOK}.
\newblock Gas Processors Suppliers Association, 13th edition, 2012.

\bibitem{article:1}
K.~Kolmetz, A.~Mardikar, H.~Gulati, W.~Kiong~Ng, T.~Yee (Ron)~Lim, and P.~S.
  Cheah.
\newblock Design guidelines for using distillation simulation software in the
  field.
\newblock {\em Asia-Pacific Journal of Chemical Engineering}, 2(4):308--314,
  2007.

\bibitem{book:design}
R.~Turton, R.~C. Bailie, W.~B. Whiting, and J.~A. Shaeiwitz.
\newblock {\em Analysis, synthesis and design of chemical processes}.
\newblock Pearson Education, 2008.

\end{thebibliography}
