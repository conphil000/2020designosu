\babel@toc {english}{}
\contentsline {section}{\numberline {1}TABLE OF CONTENTS}{I}% 
\contentsline {section}{\numberline {A}ABSTRACT}{1}% 
\contentsline {section}{\numberline {B}INTRODUCTION}{2}% 
\contentsline {section}{\numberline {C}PROCESS FLOW DIAGRAM AND MATERIAL BALANCE}{5}% 
\contentsline {section}{\numberline {D}PROCESS DESCRIPTION}{7}% 
\contentsline {subsection}{\numberline {D.a}Upstream Process}{7}% 
\contentsline {subsubsection}{\numberline {D.a.1}Nitrogen Production}{7}% 
\contentsline {subsubsection}{\numberline {D.a.2}Hydrogen Production}{8}% 
\contentsline {subsection}{\numberline {D.b}Downstream Process}{9}% 
\contentsline {subsubsection}{\numberline {D.b.1}The Haber-Bosch Process}{9}% 
\contentsline {subsection}{\numberline {D.c}Catalyst and Recycling}{10}% 
\contentsline {subsection}{\numberline {D.d}Wind Energy}{10}% 
\contentsline {section}{\numberline {E}ENERGY BALANCE AND UTILITY REQUIREMENTS}{11}% 
\contentsline {section}{\numberline {F}EQUIPMENT LIST AND UNIT DESCRIPTION}{12}% 
\contentsline {subsection}{\numberline {F.a}Design Philosophy}{12}% 
\contentsline {subsection}{\numberline {F.b}Equipment Specification}{14}% 
\contentsline {subsection}{\numberline {F.c}Equipment Cost Summary}{15}% 
\contentsline {section}{\numberline {G}FIXED CAPITAL INVESTMENT SUMMARY}{16}% 
\contentsline {section}{\numberline {H}SAFETY, HEALTH, AND ENVIRONMENTAL CONSIDERATIONS}{17}% 
\contentsline {subsection}{\numberline {H.a}Occupational Safety \& Hazard Identification}{18}% 
\contentsline {subsubsection}{\numberline {H.a.1}Safety Assessment Results}{19}% 
\contentsline {section}{\numberline {I}PROCESS SAFETY CONSIDERATIONS}{20}% 
\contentsline {subsection}{\numberline {I.a}Process Safety}{20}% 
\contentsline {subsection}{\numberline {I.b}Existing Safeguards}{23}% 
\contentsline {subsection}{\numberline {I.c}Opportunities for Additional ISD in Detailed Design}{23}% 
\contentsline {subsection}{\numberline {I.d}Industrial Health}{24}% 
\contentsline {section}{\numberline {J}ECONOMIC ANALYSIS}{26}% 
\contentsline {subsection}{\numberline {J.a}Utility/Revenue Streams}{26}% 
\contentsline {subsubsection}{\numberline {J.a.1}Utility Costs}{26}% 
\contentsline {subsubsection}{\numberline {J.a.2}Ammonia Costs}{27}% 
\contentsline {subsection}{\numberline {J.b}Capital Investments}{28}% 
\contentsline {subsection}{\numberline {J.c}Net Present Value Analysis}{29}% 
\contentsline {subsection}{\numberline {J.d}Sensitivity Analysis}{30}% 
\contentsline {subsection}{\numberline {J.e}Cash Flow Analysis}{31}% 
\contentsline {section}{\numberline {K}CONCLUSIONS}{32}% 
\contentsline {section}{\numberline {L}RECOMMENDATIONS}{32}% 
\contentsline {section}{\numberline {M}ACKNOWLEDGMENTS}{32}% 
\contentsline {section}{\numberline {A}APPENDIX (Aspen Stream Tables and PFDs)}{34}% 
\contentsline {section}{\numberline {B}APPENDIX (Equipment Costing)}{46}% 
\contentsline {section}{\numberline {C}APPENDIX (Aspen Report)}{50}% 
